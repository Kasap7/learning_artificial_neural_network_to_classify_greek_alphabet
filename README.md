# Learning_artificial_neural_network_to_classify_greek_alphabet

This program offers user GUI to draw first 5 letters of greek alphabet (alpha, beta, gamma, delta, epsilon). 
After user draws the letters program trains artificial feedforward neural network with backpropagation algorithm. 
After the learning process is done, user is again been asked to draw a letter (this time random letter), 
and program has to determine which letter is being drawn by asking artificial feedforward neural network.

Folders data.in, data1.in, data2.in, data3.in represent folders which computer generates after he asks user to draw each letter multiple times for learning purposes.
You can create your own files by drawing each letter multiple times, or if you want to skip this part you can just use one of the predefined files.

For more information about the program please see zad5.pdf.