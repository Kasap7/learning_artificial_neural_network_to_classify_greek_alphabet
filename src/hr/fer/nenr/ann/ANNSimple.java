package hr.fer.nenr.ann;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class ANNSimple {
	private Map<Double, Double> dataset = new HashMap<>();
	private Map<Windex, Double> wMap = new HashMap<>();
	private Map<Yindex, Double> yMap = new HashMap<>();
	private Map<Yindex, Double> dMap = new HashMap<>();
	private int[] arch = {1,6,1};
	private double errThr = 0.000000001;
	private double n = 1.6;
	public Random r = new Random();
	private int N, iterCount;
	
	public ANNSimple(double n, int[] arch, int iterCount) {
		this.n=n;
		this.arch = arch;
		this.iterCount = iterCount;
		// {(-1,1), (-0.8, 0.64), (-0.6,0.36), (-0.4,0.16), (-0.2,0.04), (0,0),
		//  (0.2,0.04), (0.4,0.16), (0.6,0.36), (0.8,0.64), (1,1)}
		
		dataset.put(-1.0,1.0);
		dataset.put(-0.8, 0.64);
		dataset.put(-0.6,0.36);
		dataset.put(-0.4,0.16);
		dataset.put(-0.2,0.04);
		
		dataset.put(0.0,0.0);
		
		dataset.put(0.2,0.04);
		dataset.put(0.4,0.16);
		dataset.put(0.6,0.36);
		dataset.put(0.8,0.64);
		dataset.put(1.0,1.0);
		
		dataset.put(0.9,0.81);
		
		N = dataset.size();
		
		for (int k=0; k<arch.length - 1; k++) {
			int lI = arch[k];
			int lJ = arch[k+1];
			
			for (int j=0; j<lJ; j++) {
				for (int i=0; i<lI; i++) {
					Windex wInd = new Windex(k, i, j);
					double w = r.nextDouble()*0.8 - 0.4;
					wMap.put(wInd, w);
				}
				
				Windex wInd = new Windex(k, -1, j);
				double w = r.nextDouble()*0.8 - 0.4;
				wMap.put(wInd, w);
			}
		}
	}
	
	public void learnANN() {
		int epohe = 0;
		
		double err = 10;
		while(err > errThr && epohe < iterCount) {
			if (epohe % 10000 == 0) {
				err = getSqareError();
				System.out.printf("Epohe: %d, Error: %.9f\n", epohe, err);
			}
			
			// evaluate ANN for all inputs
			int q=0;
			for (double x : dataset.keySet()) {
				double[] xArr = new double[1];
				xArr[0] = x;
				evaluate(xArr, q);
				q++;
			}
			
			// update d errors for each y value
			for (int k = arch.length-1; k>0; k--) {
				int lJ = arch[k];
				if (k == arch.length-1) {
					int s=0;
					for (Double x : dataset.keySet()) {
						for (int j=0; j<lJ; j++) {
							Yindex yInd = new Yindex(k, s, j);
							double y_ksj = yMap.get(yInd);
							double t_sj = dataset.get(x);
							double d = y_ksj * (1 - y_ksj) * (t_sj - y_ksj);
							dMap.put(yInd, d);
						}
						s++;
					}
				} else {
					for (int s=0; s<dataset.size(); s++) {
						for (int j=0; j<lJ; j++) {
							Yindex yInd = new Yindex(k, s, j);
							double y_ksj = yMap.get(yInd);
							
							double sum = 0;
							int lO = arch[k+1];
							for (int o=0; o<lO; o++) {
								Yindex yInd_o = new Yindex(k+1, s, o);
								Windex wInd = new Windex(k, j, o);
								double d_so = dMap.get(yInd_o);
								double w_jo = wMap.get(wInd);
								sum += d_so*w_jo;
							}
							double d = y_ksj * (1 - y_ksj) * sum;
							dMap.put(yInd, d);
						}
					}
				}
			}
			
			// Update w values
			for (int k = arch.length-2; k>=0; k--) {
				int lI = arch[k];
				int lJ = arch[k+1];
				
				for (int j=0; j<lJ; j++) {
					for (int i=0; i<lI; i++) {
						Windex wInd = new Windex(k, i, j);
						double w = wMap.get(wInd);
						
						double sum = 0;
						for (int s=0; s<N; s++) {
							Yindex yInd = new Yindex(k, s, i);
							Yindex dInd = new Yindex(k+1, s, j);
							sum += dMap.get(dInd) * yMap.get(yInd);
						}
						
						wMap.put(wInd, w + n*sum);
					}
					
					Windex wInd = new Windex(k, -1, j);
					double w = wMap.get(wInd);
					
					double sum = 0;
					for (int s=0; s<N; s++) {
						double yVal = 1;
						Yindex dInd = new Yindex(k+1, s, j);
						sum += dMap.get(dInd) * yVal;
					}
					
					wMap.put(wInd, w + n*sum);
				}
			}
			
			epohe++;
		}
	}
	
	public double getError() {
		double err = getSqareError();
		err = (1.0 / (2.0 * N)) * err;
		return err;
	}
	
	public double getSqareError() {
		double err = 0;
		int s=0;
		for (Double x : dataset.keySet()) {
			double[] xDouble = new double[1];
			xDouble[0] = x;
			double[] y = evaluate(xDouble, s);
			double t = dataset.get(x);
			err += (t-y[0])*(t-y[0]);
			s++;
		}
		return err;
	}
	
	private double[] evaluate(double[] x, int s) {
		for (int j=0; j<x.length; j++) {
			Yindex yInd = new Yindex(0, s, j);
			yMap.put(yInd, x[j]);
		}
		
		for (int k=1; k<arch.length; k++) {
			int lI = arch[k-1];
			int lJ = arch[k];
			
			for (int j=0; j<lJ; j++) {
				double net = 0;
				for (int i=0; i<lI; i++) {
					Windex wInd = new Windex(k-1, i, j);
					Yindex yInd = new Yindex(k-1, s, i);
					double wVal = wMap.get(wInd);
					double xVal = yMap.get(yInd);
					net += wVal*xVal;
				}
				Windex wInd = new Windex(k-1, -1, j);
				double w0 = wMap.get(wInd);
				net += w0*1;
				
				double yVal = f(net);
				Yindex yInd = new Yindex(k, s, j);
				yMap.put(yInd, yVal);
			}
		}
		
		double[] y = new double[arch[arch.length-1]];
		for (int i=0; i<y.length; i++) {
			Yindex yInd = new Yindex(arch.length-1, s, i);
			y[i] = yMap.get(yInd);
		}
		return y;
	}
	
	public double[] evaluate(double[] x) {
		return evaluate(x, -1);
	}
	
	private double f(double x) {
		return 1/(1 + Math.pow(Math.E, -x));
	}
	
	static class Windex {
		// Layer in ANN
		int k;
		
		// Starting index of a link
		int i;
		
		// Ending index of a link
		int j;
		
		public Windex(int k, int i, int j) {
			this.k=k;
			this.i=i;
			this.j=j;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + i;
			result = prime * result + j;
			result = prime * result + k;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Windex other = (Windex) obj;
			if (i != other.i)
				return false;
			if (j != other.j)
				return false;
			if (k != other.k)
				return false;
			return true;
		}
		
		@Override
		public String toString() {
			return String.format("(%d %d %d)", k, i, j);
		}
	}
	
	static class Yindex {
		// Layer in ANN
		int k;
		
		// index of dataset
		int s;
		
		// index of a node
		int i;
		
		public Yindex(int k, int s, int i) {
			this.k=k;
			this.s=s;
			this.i=i;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + i;
			result = prime * result + k;
			result = prime * result + s;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Yindex other = (Yindex) obj;
			if (i != other.i)
				return false;
			if (k != other.k)
				return false;
			if (s != other.s)
				return false;
			return true;
		}
		
		@Override
		public String toString() {
			return String.format("(%d %d %d)", k, s, i);
		}
	}
}
