package hr.fer.nenr.ann;

public class Test2 {

	public static void main(String[] args) {
		int[] arch = {1, 4, 6, 4, 1};
		double n = 1.82;
		int iter = 1000000;
		
		ANNSimple ann = new ANNSimple(n, arch, iter);
		ann.learnANN();
		
		System.out.println();
		System.out.println("x\t\tx*x\t\tANN(x)");
		for (double x=-1; x<1; x+=0.1) {
			double[] xArr = new double[1];
			xArr[0] = x;
			double[] y = ann.evaluate(xArr);
			System.out.printf("%f\t%f\t%f\n", x, x*x, y[0]);
		}
		
	}

}
