package hr.fer.nenr.ann;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
public class PictureTester extends JDialog {
	
	private static final long serialVersionUID = 1L;
	private int M;
	private int k = 0;
	private ANN ann;
	private List<double[]> dataset = new ArrayList<>();
	
	public static final String[] ALPHABET = {"Alpha", "Beta", "Gamma", "Delta", "Epsilon"};
	
	public PictureTester(ANN ann) throws IOException {
		super((Window)null);
		setModal(true);
		
		M = ann.getM();
		this.ann = ann;
		for (double[] x : ann.getDataset().keySet()) {
			dataset.add(x);
		}
		
		this.setResizable(false);
		initiate();
	}
	
	private void initiate() {
		this.setTitle("Draw dataset");
        this.setSize(850, 850);
        this.setLocation(80, 80);

        @SuppressWarnings("serial")
		final JPanel drawPan = new JPanel() {
	        
	        @Override
	        public void paint(Graphics g) {
	            super.paint(g);
	            g.setColor(Color.RED);
	            
	            double[] x = dataset.get(k);
	            
	            List<Point> dots = new ArrayList<>();
	            for (int i=0; i<M; i++) {
	            	double xD = x[2*i]*300 + 300 + 10;
	            	double yD = x[2*i+1]*300 + 300 + 10;
	            	int intX = round(xD);
	            	int intY = round(yD);
	            	dots.add(new Point(intX, intY));
	            }
	            
	            for (int i=0; i<M-1; i++) {
	            	Point a = dots.get(i);
	            	Point b = dots.get(i + 1);
	            	g.drawLine((int)a.getX(), (int)a.getY(), (int)b.getX(), (int)b.getY());
	            }
	        }

			private int round(double x) {
				int intX = (int)x;
				if (x-intX < 0.5)
					return intX;
				return intX+1;
			}
        };
        
        double[] y = ann.evaluate(dataset.get(k));
        int letter = getLetter(y);
        
        JPanel bottomPan = new JPanel();
        JButton next = new JButton("NEXT");
        final JLabel labelResult = new JLabel(String.format("Letter: %s;\t\t Output = [%.3f, %.3f, %.3f, %.3f, %.3f]"
        		, ALPHABET[letter], y[0], y[1], y[2], y[3], y[4]));
        labelResult.setFont(new Font("Serif", Font.PLAIN, 24));
        
        bottomPan.add(next);
        bottomPan.add(labelResult);
        
        this.getContentPane().add(BorderLayout.CENTER, drawPan);
        this.getContentPane().add(BorderLayout.SOUTH, bottomPan);
        
        next.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				k = (k+1) % 100;
				drawPan.repaint();
				double[] x = dataset.get(k);
				double[] y = ann.evaluate(x);
		        int letter = getLetter(y);
		        
		        labelResult.setText(String.format("Letter: %s;\t\t Output = [%.3f, %.3f, %.3f, %.3f, %.3f]"
		        		, ALPHABET[letter], y[0], y[1], y[2], y[3], y[4]));
		        
		        labelResult.repaint();
		        
		        /*
		        System.out.println(Utils.arrayToString(x));
		        System.out.println(Utils.arrayToString(y));
		        System.out.println();
		        */
			}
        });
	}
	
	private int getLetter(double[] y) {
		int letter = 0;
		double val = -5;
		for (int i=0; i<y.length; i++) {
			if (y[i] > val) {
				val = y[i];
				letter = i;
			}
		}
		return letter;
	}
    
    static class D {
    	D(double x, double y) {
    		this.x = x;
    		this.y = y;
    	}
    	double x, y;
    }
}
