package hr.fer.nenr.ann;

import javax.swing.*;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.ArrayList;
import java.util.List;
public class PictureValidate extends JDialog {
	
	private static final long serialVersionUID = 1L;
	
	private List<Point> points = new ArrayList<>();
	private int M;
	private boolean draw = false;
	private ANN ann;
	private double[] x = null;
	private double[] y = null;
	
	public static final String[] ALPHABET = {"Alpha", "Beta", "Gamma", "Delta", "Epsilon"};
	
	public PictureValidate(ANN ann) {
		super((Window)null);
		setModal(true);
		
		this.M = ann.getM();
		this.ann = ann;
		
		this.setResizable(false);
		initiate();
	}
	
	private void initiate() {
		this.setTitle("Testing the ANN");
        this.setSize(850, 480);
        this.setLocation(50, 50);
        
      //Creating the panel at bottom and adding components
        JPanel panel = new JPanel(); // the panel is not visible in output
        final JLabel label = new JLabel(String.format("Please draw alphabetic letter"));
        label.setFont(new Font("Serif", Font.PLAIN, 24));
        panel.add(label); // Components Added using Flow Layout

        @SuppressWarnings("serial")
		final JPanel transformedDrawPan = new JPanel() {
        	@Override
	        public void paint(Graphics g) {
	            super.paint(g);
	            if (draw == false && x != null) {
	                g.setColor(Color.RED);
	                
	                List<Point> dots = new ArrayList<>();
		            for (int i=0; i<M; i++) {
		            	double xD = x[2*i]*180 + 180 + 10;
		            	double yD = x[2*i+1]*180 + 180 + 10;
		            	int intX = Utils.round(xD);
		            	int intY = Utils.round(yD);
		            	dots.add(new Point(intX, intY));
		            }
		            
		            for (int i=0; i<M-1; i++) {
		            	Point a = dots.get(i);
		            	Point b = dots.get(i + 1);
		            	g.drawLine((int)a.getX(), (int)a.getY(), (int)b.getX(), (int)b.getY());
		            }
		            
		            /*
		            System.out.println(Utils.arrayToString(x));
			        System.out.println(Utils.arrayToString(y));
			        System.out.println();
			        */
	            }
	        }
        };
        
        @SuppressWarnings("serial")
		JPanel drawPan = new JPanel() {
	        {
	            addMouseListener(new MouseAdapter() {
	                public void mousePressed(MouseEvent e) {
	                    draw = true;
	                    Point p = e.getPoint();
	    				points.add(p);
	                }

	                public void mouseReleased(MouseEvent e) {
	                    draw = false;
	                    x = transformPoints();
	                    y = ann.evaluate(x);
	                    int letter = getLetter(y);
	                    double[] prob = Utils.getProbability(y);
	                    
	                    label.setText(String.format("Letter: %s;\t\t Output = [%.3f, %.3f, %.3f, %.3f, %.3f]"
	                    		, ALPHABET[letter], prob[0], prob[1], prob[2], prob[3], prob[4]));
	                    label.repaint();
	                    transformedDrawPan.repaint();
	                    points.clear();
	                }
	                
					private int getLetter(double[] y) {
						int letter = 0;
						double val = -5;
						for (int i=0; i<y.length; i++) {
							if (y[i] > val) {
								val = y[i];
								letter = i;
							}
						}
						return letter;
					}

					private double[] transformPoints() {
						double m = 0, xTc = 0, yTc = 0;
						for (Point p : points) {
							xTc += p.getX();
							yTc += p.getY();
						}
						
						xTc /= points.size();
						yTc /= points.size();
						
						List<D> dots = new ArrayList<>();
						
						for (Point p : points) {
							D d = new D((p.getX() - xTc), (p.getY() - yTc));
							
							if (Math.abs(d.x) > m)
								m = Math.abs(d.x);
							if (Math.abs(d.y) > m)
								m = Math.abs(d.y);
							
							dots.add(d);
						}
						
						for (D d : dots) {
							d.x = d.x/m;
							d.y = d.y/m;
						}
						
						double length = 0;
						
						for (int i=0; i<dots.size()-1; i++) {
							D d1 = dots.get(i), d2 = dots.get(i+1);
							length += Utils.getLength(d1, d2);
						}
						
						List<D> dotList = new ArrayList<>();
						dotList.add(dots.get(0));
						
						double l = 0;
						int i=0;
						D d1 = dots.get(i), d2 = dots.get(i+1);
						for (int k=1; k<M; k++) {
							while (l < (k*length)/(M-1)) {
								d1 = dots.get(i);
								i++;
								if (i >= dots.size()) {
									break;
								}
								d2 = dots.get(i);
								l += Utils.getLength(d1, d2);
							}
							double l_ = l - Utils.getLength(d1, d2);
							double dL1 = Math.abs((k*length)/(M-1) - l_);
							double dL2 = Math.abs(l - (k*length)/(M-1));
							if (dL1 < dL2) {
								dotList.add(d1);
							} else {
								dotList.add(d2);
							}
						}
						
						double[] x = new double[2*M];
						int k = 0;
						for (D d : dotList) {
							x[k++] = d.x;
							x[k++] = d.y;
						}
						
						return x;
							
					}
					
	            });
	            addMouseMotionListener(new MouseMotionAdapter() {
	                public void mouseMoved(MouseEvent e) {
	                	
	                }

	                public void mouseDragged(MouseEvent e) {
	                	Point p = e.getPoint();
	    				points.add(p);
	                    repaint();
	                }
	            });
	        }
	        
	        @Override
	        public void paint(Graphics g) {
	            super.paint(g);
	            if (draw) {
	                g.setColor(Color.RED);
	                for (int i=0; i<points.size()-1; i++) {
	                	Point p1 = points.get(i), p2 = points.get(i+1);
	                	g.drawLine(p1.x, p1.y, p2.x, p2.y);
	                }
	            }
	        }
        };
        
        JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, drawPan, transformedDrawPan);
        splitPane.setDividerLocation(420);
		splitPane.setEnabled(false);
		
		//Provide minimum sizes for the two components in the split pane
		Dimension minimumSize = new Dimension(300, 300);
		drawPan.setMinimumSize(minimumSize);
		transformedDrawPan.setMinimumSize(minimumSize);

        //Adding Components to the frame.
        this.getContentPane().add(BorderLayout.SOUTH, panel);
        this.getContentPane().add(BorderLayout.CENTER, splitPane);
	}
    
    static class D {
    	D(double x, double y) {
    		this.x = x;
    		this.y = y;
    	}
    	double x, y;
    }
}
