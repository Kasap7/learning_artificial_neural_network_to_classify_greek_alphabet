package hr.fer.nenr.ann;

import hr.fer.nenr.ann.PictureValidate.D;

public class Utils {

	public static String arrayToString(double[] x){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		for (int i=0; i<x.length-1; i++) {
			sb.append(String.format("%.4f ", x[i]));
		}
		sb.append(String.format("%.4f]", x[x.length-1]));
		return sb.toString();
	}
	
	public static int round(double x) {
		int intX = (int)x;
		if (x - intX < 0.5)
			return intX;
		return intX+1;
	}
	
	public static double getLength(D d1, D d2) {
		return Math.sqrt( (d1.x-d2.x)*(d1.x-d2.x) + (d1.y-d2.y)*(d1.y-d2.y) );
	}
	
	public static double[] getProbability(double[] y) {
		double sum = 0;
		double[] yProb = new double[y.length];
		
		for (int i=0; i<y.length; i++) {
			sum += y[i];
		}
		for (int i=0; i<y.length; i++) {
			yProb[i] = y[i]/sum;
		}
		
		return yProb;
	}
}
