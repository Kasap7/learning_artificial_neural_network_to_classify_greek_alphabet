package hr.fer.nenr.ann;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import javax.swing.*;

public class Test {

	public static void main(String args[]) throws Exception {
		
		int[] arch = {60, 2, 5};
		double errThr = 0.000000001;
		List<String> lines = Files.readAllLines(Paths.get("data2.in"));
		
		ANN ann = new ANN(arch, lines, errThr);
		ann.learnMiniBatch();
		System.out.println();
		
		SwingUtilities.invokeAndWait(new Runnable() {
			@Override
			public void run() {
				PictureTester pt;
				try {
					pt = new PictureTester(ann);
					pt.setVisible(true);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}); 
	}
}
