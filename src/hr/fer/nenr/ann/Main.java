package hr.fer.nenr.ann;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

import javax.swing.SwingUtilities;

public class Main {
	
	public static PictureFrame pic;

	public static void main(String[] args) throws InvocationTargetException, InterruptedException, IOException {
		Scanner s = new Scanner(System.in);
		String line;
		
		System.out.println("Do you want to create new dataset [y|n]: ");
		line = s.nextLine().trim();
		
		if (line.equalsIgnoreCase("y")) {
			
			System.out.println("Dataset file-name: ");
			line = s.nextLine().trim();
			final String file = line;
			
			System.out.println("Dataset point counts [M]: ");
			line = s.nextLine().trim();
			final int M = Integer.parseInt(line);
			
			SwingUtilities.invokeAndWait(new Runnable() {

				@Override
				public void run() {
					pic = new PictureFrame(M);
					pic.setVisible(true);
					String output = pic.getResult();
					//System.out.println(output);
					try (PrintWriter out = new PrintWriter(file)) {
					    out.println(output);
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}
				}
				
			});
		}
		
		System.out.println("Please enter file name that contains dataset: ");
		line = s.nextLine().trim();
		File file = new File(line);
		while (!file.exists()) {
			System.out.println("Please enter file name that contains dataset: ");
			line = s.nextLine().trim();
			file = new File(line);
		}
		
		int M2 = 0;
		List<String> lines = Files.readAllLines(Paths.get(line));
		String[] lineArr2 = lines.get(0).trim().split("\\s+");
		M2 = lineArr2.length - 5;
		
		boolean done = false;
		int[] arch = null;
		while (!done) {
			System.out.printf("Please enter the architecture of ANN (%d x_2 x_3 ... x_k-1 x_k 5): \n", M2);
			line = s.nextLine().trim();
			String[] lineArr = line.split("\\s+");
			arch = new int[lineArr.length];
			
			try {
				for (int i=0; i<lineArr.length; i++) {
					arch[i] = Integer.parseInt(lineArr[i]);
				}
				
				if (arch[0] == M2 && arch[arch.length-1] == 5 && arch.length >= 3) {
					done = true;
				} 
			} catch(Exception e) {}
		}
		
		System.out.println("Please enter Error threshold: ");
		line = s.nextLine().trim();
		double errThr = Double.parseDouble(line);
		
		s.close();
		
		/*
		int[] arch = {60, 10, 5, 10, 5};
		double errThr = 0.000000001;
		List<String> lines = Files.readAllLines(Paths.get("data2.in"));
		*/
		
		ANN ann = new ANN(arch, lines, errThr);
		ann.learnMiniBatch();
		
		SwingUtilities.invokeAndWait(new Runnable() {
			@Override
			public void run() {
				PictureValidate pv = new PictureValidate(ann);
				pv.setVisible(true);
			}
		});
	}

}
