package hr.fer.nenr.ann;

import javax.swing.*;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.ArrayList;
import java.util.List;
public class PictureFrame extends JDialog {
	
	private static final long serialVersionUID = 1L;
	
	private List<Point> points = new ArrayList<>();
	private int M, k = 20, letter = 0;
	private boolean draw = false;
	private StringBuilder sb = new StringBuilder();
	
	public static final String[] ALPHABET = {"Alpha", "Beta", "Gamma", "Delta", "Epsilon"};
	
	public PictureFrame(int M) {
		super((Window)null);
		setModal(true);
		
		this.M = M;
		
		this.setResizable(false);
		initiate();
	}
	
	private void initiate() {
		this.setTitle("Draw dataset");
        this.setSize(800, 600);
        this.setLocation(200, 200);
        
      //Creating the panel at bottom and adding components
        JPanel panel = new JPanel(); // the panel is not visible in output
        final JLabel label = new JLabel(String.format("Letter: %s; \t\t Count: %d", ALPHABET[letter], k));
        label.setFont(new Font("Serif", Font.PLAIN, 24));
        panel.add(label); // Components Added using Flow Layout

        @SuppressWarnings("serial")
		JPanel drawPan = new JPanel() {
	        {
	            addMouseListener(new MouseAdapter() {
	                public void mousePressed(MouseEvent e) {
	                    draw = true;
	                    Point p = e.getPoint();
	    				points.add(p);
	                }

	                public void mouseReleased(MouseEvent e) {
	                    draw = false;
	                    transformPoints();
	                    k--;
	                    if (k<=0) {
	                    	k = 20;
	                    	letter++;
	                    	if (letter >= 5) {
	                    		exitFrame();
	                    		return;
	                    	}
	                    }
	                    
	                    if (letter >= 5) {
	                    	exitFrame();
                    		return;
	                    }
	                    
	                    label.setText(String.format("Letter: %s; \t\t Count: %d", ALPHABET[letter], k));
	                    label.repaint();
	                    points.clear();
	                }

					private void transformPoints() {
						double m = 0, xTc = 0, yTc = 0;
						for (Point p : points) {
							xTc += p.getX();
							yTc += p.getY();
						}
						
						xTc /= points.size();
						yTc /= points.size();
						
						List<D> dots = new ArrayList<>();
						
						for (Point p : points) {
							D d = new D((p.getX() - xTc), (p.getY() - yTc));
							
							if (Math.abs(d.x) > m)
								m = Math.abs(d.x);
							if (Math.abs(d.y) > m)
								m = Math.abs(d.y);
							
							dots.add(d);
						}
						
						for (D d : dots) {
							d.x = d.x/m;
							d.y = d.y/m;
						}
						
						double length = 0;
						
						for (int i=0; i<dots.size()-1; i++) {
							D d1 = dots.get(i), d2 = dots.get(i+1);
							length += getLength(d1, d2);
						}
						
						List<D> dotList = new ArrayList<>();
						dotList.add(dots.get(0));
						
						double l = 0;
						int i=0;
						D d1 = dots.get(i), d2 = dots.get(i+1);
						for (int k=1; k<M; k++) {
							while (l < (k*length)/(M-1)) {
								d1 = dots.get(i);
								i++;
								if (i >= dots.size()) {
									break;
								}
								d2 = dots.get(i);
								l += getLength(d1, d2);
							}
							double l_ = l - getLength(d1, d2);
							double dL1 = Math.abs((k*length)/(M-1) - l_);
							double dL2 = Math.abs(l - (k*length)/(M-1));
							if (dL1 < dL2) {
								dotList.add(d1);
							} else {
								dotList.add(d2);
							}
						}
						
						for (D d : dotList) {
							sb.append(String.format("%f %f ", d.x, d.y));
						}
						
						int a1=0, a2=0, a3=0, a4=0, a5=0;
						a1 = letter == 0 ? 1 : 0;
						a2 = letter == 1 ? 1 : 0;
						a3 = letter == 2 ? 1 : 0;
						a4 = letter == 3 ? 1 : 0;
						a5 = letter == 4 ? 1 : 0;
						sb.append(String.format("%d %d %d %d %d\n", a1, a2, a3, a4, a5));
							
					}

					private double getLength(D d1, D d2) {
						return Math.sqrt( (d1.x-d2.x)*(d1.x-d2.x) + (d1.y-d2.y)*(d1.y-d2.y) );
					}
	            });
	            addMouseMotionListener(new MouseMotionAdapter() {
	                public void mouseMoved(MouseEvent e) {
	                	
	                }

	                public void mouseDragged(MouseEvent e) {
	                	Point p = e.getPoint();
	    				points.add(p);
	                    repaint();
	                }
	            });
	        }
	        
	        @Override
	        public void paint(Graphics g) {
	            super.paint(g);
	            if (draw) {
	                g.setColor(Color.RED);
	                for (int i=0; i<points.size()-1; i++) {
	                	Point p1 = points.get(i), p2 = points.get(i+1);
	                	g.drawLine(p1.x, p1.y, p2.x, p2.y);
	                }
	            }
	        }
        };

        //Adding Components to the frame.
        this.getContentPane().add(BorderLayout.SOUTH, panel);
        this.getContentPane().add(BorderLayout.CENTER, drawPan);
	}
	
	private void exitFrame() {
		this.dispose();
	}
	

	public String getResult() {
		return sb.toString();
	}
    
    static class D {
    	D(double x, double y) {
    		this.x = x;
    		this.y = y;
    	}
    	double x, y;
    }
}
