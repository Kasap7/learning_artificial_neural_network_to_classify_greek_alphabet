package hr.fer.nenr.ann;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

public class ANN {
	private static final double n_start = 0.023;
	private static final double err_min_thr = 0.18;
	
	private Map<double[], double[]> dataset = new LinkedHashMap<>();
	private List<List<double[]>> groupsBatch = new ArrayList<>();
	private List<List<double[]>> groupsMiniBatch = new ArrayList<>();
	private List<List<double[]>> groupsStohastic = new ArrayList<>();
	private int[] arch;
	private double errThr;
	private double n = n_start;
	private Random r = new Random();
	private int iterCount = 50000, N;
	private Map<Windex, Double> wMap = new HashMap<>();
	private Map<Yindex, Double> yMap = new HashMap<>();
	private Map<Yindex, Double> dMap = new HashMap<>();
	
	public ANN(int[] arch, List<String> datasetList, double errThr) {
		this.arch = arch;
		this.errThr = errThr;
		
		String[] lineArr = datasetList.get(0).trim().split("\\s+");
		int M2 = lineArr.length - 5;
		
		for (String line : datasetList) {
			line = line.trim();
			if (line.isEmpty())
				break;
			
			lineArr = line.split("\\s+");
			
			double[] dots = new double[M2];
			for (int i=0; i<M2; i++) {
				dots[i] = Double.parseDouble(lineArr[i]);
			}
			
			double[] letter = new double[5];
			for (int i=0; i<5; i++) {
				letter[i] = Integer.parseInt(lineArr[M2 + i]);
			}
			
			dataset.put(dots, letter);
		}
		
		N = dataset.size();
		
		initWeights();
		initDataset();
	}

	private void initDataset() {
		Set<double[]> keys = dataset.keySet();
		List<double[]> batch = new ArrayList<>();
		for (double[] key : keys) {
			batch.add(key);
			
			List<double[]> l = new ArrayList<>();
			l.add(key);
			groupsStohastic.add(l);
		}
		
		groupsBatch.add(batch);
		
		int block = batch.size() / arch[arch.length-1];
				
		for (int i=0; i<batch.size()/10; i++) {
			List<double[]> minibatch = new ArrayList<>();
			for (int m=0; m<arch[arch.length-1]; m++) {
				minibatch.add(batch.get(m*block + i*2 + 0));
				minibatch.add(batch.get(m*block + i*2 + 1));
			}
			groupsMiniBatch.add(minibatch);
		}
	}

	private void initWeights() {
		for (int k=0; k<arch.length - 1; k++) {
			int lI = arch[k];
			int lJ = arch[k+1];
			
			for (int j=0; j<lJ; j++) {
				for (int i=0; i<lI; i++) {
					Windex wInd = new Windex(k, i, j);
					double w = r.nextDouble()*0.8 - 0.4;
					wMap.put(wInd, w);
				}
				
				Windex wInd = new Windex(k, -1, j);
				double w = r.nextDouble()*0.8 - 0.4;
				wMap.put(wInd, w);
			}
		}
	}
	
	public void learnBatch() {
		learn(groupsBatch);
	}
	public void learnMiniBatch() {
		learn(groupsMiniBatch);
	}
	public void learnStohastic() {
		learn(groupsStohastic);
	}
	
	private void learn(List<List<double[]>> groups) {
		int epohe = 0;
		int epoheGroups = 0;
		
		double err = getMAE();
		double errPrev = err;
		int countingErrors = 0;
		
		while(err > errThr && epohe < iterCount) {
			
			if (epohe % 100 == 0) {
				err = getMAE();
				System.out.printf("Epohe: %d, EpoheGroups: %d, Error: %.9f, Learning step = %f\n", epohe, epoheGroups, err, n);
				
			}
			
			if (epohe % 10 == 0) {
				errPrev = err;
				err = getMAE();
				
				
				if (err < err_min_thr) {
					countingErrors = 0;
					if (err > errPrev*0.75) {
						n = n * 2;
					} else if (err < errPrev) {
						n = n * 1.075;
					} else if (err >= errPrev) {
						n = n * 0.6;
					}
				} else if (err >= err_min_thr && err <= 1) {
					n = n_start * (2 - err);
					countingErrors++;
					if (countingErrors >= 20) {
						n = n_start*10;
					}
				} else {
					n = n_start;
				}
			}
			
			Collections.shuffle(groups, r);
			for (int m=0; m<groups.size(); m++) {
				List<double[]> group = groups.get(m);
				
				// evaluate ANN for every input in the group
				for (int s_=0; s_<group.size(); s_++) {
					double[] x = group.get(s_);
					int s = m*group.size() + s_;
					evaluate(x, s);
				}
				
				// update d errors for each y value
				for (int k = arch.length-1; k>0; k--) {
					int lJ = arch[k];
					
					if (k == arch.length-1) {
						int s_=0;
						for (double[] x : group) {
							for (int j=0; j<lJ; j++) {
								int s = m*group.size() + s_;
								Yindex yInd = new Yindex(k, s, j);
								double y_ksj = yMap.get(yInd);
								double[] t_sj = dataset.get(x);
								double d = y_ksj * (1 - y_ksj) * (t_sj[j] - y_ksj);
								dMap.put(yInd, d);
							}
							s_++;
						}
						
					} else {
						for (int s_=0; s_<group.size(); s_++) {
							for (int j=0; j<lJ; j++) {
								int s = m*group.size() + s_;
								Yindex yInd = new Yindex(k, s, j);
								double y_ksj = yMap.get(yInd);
								
								double sum = 0;
								int lO = arch[k+1];
								for (int o=0; o<lO; o++) {
									Yindex yInd_o = new Yindex(k+1, s, o);
									Windex wInd = new Windex(k, j, o);
									double d_so = dMap.get(yInd_o);
									double w_jo = wMap.get(wInd);
									sum += d_so*w_jo;
								}
								double d = y_ksj * (1 - y_ksj) * sum;
								dMap.put(yInd, d);
							}
						}
					}
				}
				
				// Update w values
				for (int k = arch.length-2; k>=0; k--) {
					int lI = arch[k];
					int lJ = arch[k+1];
					
					for (int j=0; j<lJ; j++) {
						for (int i=0; i<lI; i++) {
							Windex wInd = new Windex(k, i, j);
							double w = wMap.get(wInd);
							
							double sum = 0;
							for (int s_=0; s_<group.size(); s_++) {
								int s = m*group.size() + s_;
								Yindex yInd = new Yindex(k, s, i);
								Yindex dInd = new Yindex(k+1, s, j);
								double yVal = yMap.get(yInd);
								double dVal = dMap.get(dInd);
								sum += dVal*yVal;
							}
							
							wMap.put(wInd, w + n*sum);
						}
						
						Windex wInd = new Windex(k, -1, j);
						double w = wMap.get(wInd);
						
						double sum = 0;
						for (int s_=0; s_<group.size(); s_++) {
							int s = m*group.size() + s_;
							double yVal = 1;
							Yindex dInd = new Yindex(k+1, s, j);
							sum += dMap.get(dInd) * yVal;
						}
						
						wMap.put(wInd, w + n*sum);
					}
				}
				
				epoheGroups++;
			}
			
			epohe++;
		}
		
		err = getMAE();
		System.out.printf(">>>END. Epohe: %d, EpoheGroups: %d, Error: %.9f, Learning step = %f\n", epohe, epoheGroups, err, n);
	}
	
	public double getMAE() {
		double err = getSqareError();
		err = (1.0 / (N)) * err;
		return err;
	}
	
	public double getSqareError() {
		double err = 0;
		int s=0;
		for (double[] x : dataset.keySet()) {
			double[] y = evaluate(x, s);
			double[] t = dataset.get(x);
			for (int i=0; i<arch[arch.length-1]; i++)
				err += (t[i]-y[i])*(t[i]-y[i]);
			s++;
		}
		return err;
	}
	
	private double[] evaluate(double[] x, int s) {
		for (int j=0; j<x.length; j++) {
			Yindex yInd = new Yindex(0, s, j);
			yMap.put(yInd, x[j]);
		}
		
		for (int k=1; k<arch.length; k++) {
			int lI = arch[k-1];
			int lJ = arch[k];
			
			for (int j=0; j<lJ; j++) {
				double net = 0;
				for (int i=0; i<lI; i++) {
					Windex wInd = new Windex(k-1, i, j);
					Yindex yInd = new Yindex(k-1, s, i);
					double wVal = wMap.get(wInd);
					double xVal = yMap.get(yInd);
					net += wVal*xVal;
				}
				Windex wInd0 = new Windex(k-1, -1, j);
				double w0 = wMap.get(wInd0);
				net += w0*1;
				
				double yVal = f(net);
				Yindex yInd = new Yindex(k, s, j);
				yMap.put(yInd, yVal);
			}
		}
		
		double[] y = new double[arch[arch.length-1]];
		for (int i=0; i<y.length; i++) {
			Yindex yInd = new Yindex(arch.length-1, s, i);
			y[i] = yMap.get(yInd);
		}
		return y;
	}
	
	public double[] evaluate(double[] x) {
		return evaluate(x, -1);
	}
	
	private double f(double x) {
		return 1/(1 + Math.pow(Math.E, -x));
	}
	
	public int getM() {
		return arch[0]/2;
	}
	
	public LinkedHashMap<double[], double[]> getDataset() {
		return new LinkedHashMap<double[],double[]>(dataset);
	}
	
	static class Windex {
		// Layer in ANN
		int k;
		
		// Starting index of a link
		int i;
		
		// Ending index of a link
		int j;
		
		public Windex(int k, int i, int j) {
			this.k=k;
			this.i=i;
			this.j=j;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + i;
			result = prime * result + j;
			result = prime * result + k;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Windex other = (Windex) obj;
			if (i != other.i)
				return false;
			if (j != other.j)
				return false;
			if (k != other.k)
				return false;
			return true;
		}
		
		@Override
		public String toString() {
			return String.format("(%d %d %d)", k, i, j);
		}
	}
	
	static class Yindex {
		// Layer in ANN
		int k;
		
		// index of dataset
		int s;
		
		// index of a node
		int i;
		
		public Yindex(int k, int s, int i) {
			this.k=k;
			this.s=s;
			this.i=i;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + i;
			result = prime * result + k;
			result = prime * result + s;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Yindex other = (Yindex) obj;
			if (i != other.i)
				return false;
			if (k != other.k)
				return false;
			if (s != other.s)
				return false;
			return true;
		}
		
		@Override
		public String toString() {
			return String.format("(%d %d %d)", k, s, i);
		}
	}
}
